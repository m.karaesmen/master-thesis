# [DT80m series 3 installation reference]
# installation: 2022-08-10 13:35 UTC+2
# temperature sensor on channel 3
# connections Geolore	cables filter	cables dt80 (channel 1 and 2) 
ref	black	#
1	blue	*
2	red	+
3	green	-

# [data conversion reference]
# via direct export or dump_dbd_v13
# [data files]
# export via deX program as .dbd (binary files) and .csv (ascii) with data (_data) + alarms (_data_alarms) in time range and all data (_all)
# meta data in meta.ini
# columns in data files:
