# [LIAG Geolore Logger installation reference]
# installation: 2022-08-10 13:35 UTC+2
# Geolore connected via plugs ref, 1, 2, 3 parallel to dt80 between elektrodes and filter (see images in /users/Projekte/2022_spiekeroog/Documentation/2022/SP/)
# connections Geolore	cables filter	cables dt80 (channel 1 and 2) 
ref	black	#
1	blue	*
2	red	+
3	green	-

# [data conversion reference]
# export from TF card as GEOLORE.BIN (binary data file) and GEOLORE.GPS (GPS file, both created internally by logger)
# convert via Conv_24.exe into GEOLORE_XXXX.TXT (ascii file) and GEOLORE_XXXX.ML1 (matlab file) via predefined geolore.ini
# view via Dataview.exe

# [data files]
# meta data in meta.ini
# columns in data files:
