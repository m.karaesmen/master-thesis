Hallo ! 

DataTaker anschauen.
Rote LED "Attn" anleuchtet: Error aufgetreten, wahrscheinlich seitdem nichts gemessen. Error merken und dokumentieren ! Ein Mal auf Edit (Gelbe Knopf) drücken und dann wird der Error weggehen.  Falls kein Error da war, Daten muss geholt werden.
_________________________
mit USB-Stick Datenlesen

Um die Daten aus dem DataTaker Series 4 auf Spiekeroog zu herunterladen, muss lila USB-Stick benutzt werden. Nachdem einstecken dauert es paar minuten bis USB-Stick gelesen ist. Es ist auch einfacher zu sehen, wenn DataTaker mit Feldlaptop bereits verbunden ist. Das Programm DataTransfer zeigt die Rückmeldungen von DataTaker und wenn USB-Stick eingelesen ist, wird automatisch gemeldet.
Die Daten aus dem DataTaker werden Binär sein. Joost kennt es, man muss dump_dbd_v13 Ordner auf dem Schreibtisch benutzen !



Falls die Elektroden getauscht werden soll, so ist die Situation momentan auf Spikeroog.

West: Elektrode 11
Nord: Elektrode 4
Zentrum: Elektrode 9 & 2
____________________________________
Wichtig beim Elektroden anschließen

Schwarz: REFERENZ Elektrode. Momentan REFERENZ Elektrode ist Elektrode 9 (was auch an # geschlossen ist !)
Blau, Rot und Grün werden gefiltert, es ist egal (außer Schwarz) welche Elektrode an welchem angeschloßen ist, 
SOLANGE ES IST DOKUMENTIERT !

memos_final_script.dxc ist der richtige Skript.

original_dt80_spiekeroog_script.txt ist alte Skript von Maxi was auch zuletzt angegeben wurde.


Im DataTaker Skript sind die Variablen als Farben eingegeben, Ihr könnt es direkt nach Elektroden & Dipolrichtung umbenennen.

zB: 
		1*V("Blue_Filter",ES10,FF5) 1+V("Red_Filter",ES10,FF5) 1-V("Green_Filter",ES10,FF5)

		1*V("E9_E4_Nord_Filter",ES10,FF5) 1+V("E9_E11_West_Filter",ES10,FF5) 1-V("E9_E2_Center_Filter",ES10,FF5)



______________________________
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Bevor Skript in DataTaker angeben, ! Set Date/Time ! (siehe pdf) 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
''''''''''''''''''''''''''''''


Neue Temparatursensor aus Bonn kann mit folgendem Skript gemessen werden. Es wird in -# eingesteckt.

		3-TK("Temperatur_mV",ES30,FF7)


Alte Sensor (was auf Spiekeroog war) mit folgendem (Dieser Sensor hat 4 Stecker also *+-#)

		3PT385("Temp",4W,FF7)

